core = 7.x
api = 2

; uw_ct_engineering_case_study
projects[uw_ct_engineering_case_study][type] = "module"
projects[uw_ct_engineering_case_study][download][type] = "git"
projects[uw_ct_engineering_case_study][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_engineering_case_study.git"
projects[uw_ct_engineering_case_study][download][tag] = "7.x-1.4"
projects[uw_ct_engineering_case_study][subdir] = ""

